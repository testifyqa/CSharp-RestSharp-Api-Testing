﻿using System.Linq;
using RestAutomation.Apis;
using TechTalk.SpecFlow;

namespace RestAutomation.Utils.Hooks
{
    [Binding]
    internal static class FeatureHooks
    {
        [BeforeFeature()]
        internal static void BeforeHooks()
        {
            if (FeatureContext.Current.FeatureInfo.Tags.Contains("Api"))
            {
                BaseApiTests.SetBaseUriAndAuth();
            }
        }
    }
}
