﻿using System.Linq;
using RestAutomation.Apis;
using TechTalk.SpecFlow;

namespace RestAutomation.Utils.Hooks
{
    [Binding]
    internal static class ScenarioHooks
    {
        [BeforeScenario()]
        internal static void BeforeHooks()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("Api"))
            {
                BaseApiTests.SetBaseUriAndAuth();
            }
        }
    }
}
